# Changelog
## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- Appel de l'archiviste via `use Spip\Archiver\SpipArchiver`
- Compatible SPIP 5.0.0-dev

### Fixed

- Support de la branche 4.3 de SPIP
- #4909 Afficher les infos de nouvelle version aussi sur les plugins-dist
- HTML5: Retrait des `CDATA` et `text/javascript` dans les balises `<script>`
